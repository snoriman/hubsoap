package my.paynet.endpoint;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import my.paynet.InputRequest;
import my.paynet.ObjectFactory;
import my.paynet.OutputResponse;






@Endpoint
public class WebServiceEndpoint {

	private static final String NAMESPACE_URI = "http://paynet.my";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "inputRequest")
	@ResponsePayload
	public OutputResponse hello(@RequestPayload InputRequest request) {

		System.out.println("Request param :"+request.getParam()+request.getBankid()+request.getBiccode());
		String outputString="";
		
	/*	if(request.getParam().equals("limit")) {
			outputString="200";
		}else if(request.getParam().equals("total")) {
			outputString="240";
		}else {
			outputString="invalid request";
		}
*/
		ObjectFactory factory = new ObjectFactory();
		OutputResponse response = factory.createOutputResponse();
		response.setStatusCode("00");
		response.setStatusReason("Success");
		response.setThresholdAmber("WELCOME !!");
		response.setThresholdlevelAmber("9000");
		response.setThresholdlevelRed("12");
		response.setThresholdRed("12");
		return response;
	}
}
